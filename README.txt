http://drupal.org/project/polymaps

The Polymaps Drupal module integrates the Polymapa
Javascript web mapping library with the PHP content
management system Drupal.

See:
http://polymaps.org/
http://drupal.org/


======================================================================
Dependencies
======================================================================
This module is built on the Mapping framework, which utilizes
CTools:

http://drupal.org/project/mapping
http://drupal.org/project/ctools


======================================================================
Credits
======================================================================
Developers: Alan Palazzolo (aka zzolo)